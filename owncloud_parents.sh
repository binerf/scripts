#!/bin/sh
#
#set -x

case "$(pidof owncloudcmd | wc -w)" in

0)  echo "Starting sync:"
	owncloudcmd --trust -n --silent\
		/mnt/Images \
		https://www.mygaia.org/site/owncloud/remote.php/webdav/Images
    ;;
1)  exit 1
	#echo "Sync already in progress."
    ;;
esac

